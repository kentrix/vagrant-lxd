#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'fakefs/spec_helpers'

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/action'
require 'lib/vagrant-lxd/config'
require 'lib/vagrant-lxd/driver'

describe VagrantLXD::Driver do
  let(:lxd) { double('lxd') }
  let(:machine) { double('machine').as_null_object }
  let(:config) { VagrantLXD::Config.new.tap(&:finalize!) }

  let(:valid_map) { "both 1000 1000\nuid 50-60 500-510\ngid 70-80 700-710" }
  let(:invalid_map) { "foo 1000 1000" }
  let(:valid_both) { 1000 }
  let(:valid_uid) { 55 }
  let(:valid_gid) { 75 }
  let(:invalid_both) { 999 }

  before do
    Hyperkit::Client.stub(:new).and_return(lxd)
    machine.stub(:provider_config).and_return(config)
    config.stub(:client_certificate).and_return('/tmp/client.crt')
    config.stub(:client_key).and_return('/tmp/client.key')
  end

  subject do
    described_class.new(machine)
  end

  context 'before the machine has been created' do
    before do
      machine.stub(:id).and_return(nil)
    end

    its('info') { should be nil }
    its('state') { should be Vagrant::MachineState::NOT_CREATED_ID }
    its('vagrant_uid') { should be 1000 }
    its('vagrant_gid') { should be 1000 }
    its('synced_folders_usable?') { should be(true).or be(false) }

    describe 'config' do
      subject do
        super().send(:config)
      end

      it { should be_a Hash }

      context 'without synced folder host capability' do
        before do
          machine.stub_chain(:env, :host, :capability).with(:synced_folders).and_return(false)
        end

        it { should_not include :'raw.idmap' }
      end

      context 'with synced folder host capability' do
        before do
          machine.stub_chain(:env, :host, :capability).with(:synced_folders).and_return(true)
        end

        it { should include :'raw.idmap' }

        describe 'the id map' do
          subject do
            super().fetch(:'raw.idmap').split(/\n/)
          end

          context 'with the default uid and gid settings' do
            it { should include "uid #{Process.uid} 1000" }
            it { should include "gid #{Process.gid} 1000" }
          end

          context 'with custom uid and gid settings' do
            before do
              machine.stub_chain(:provider_config, :vagrant_uid).and_return(500)
              machine.stub_chain(:provider_config, :vagrant_gid).and_return(500)
            end

            it { should include "uid #{Process.uid} 500" }
            it { should include "gid #{Process.gid} 500" }
          end

          context 'with an explicit id map' do
            before do
              machine.stub(:provider_config) do
                VagrantLXD::Config.new.tap do |c|
                  c.config = { 'raw.idmap': "uid 1 1" }
                  c.finalize!
                end
              end
            end

            it { should eq [ "uid 1 1" ] }
          end
        end
      end
    end

    describe 'id_in_map?' do
      context 'with valid id type and value' do
        it 'should equal true' do
          subject.send(:id_in_map?, valid_both, 'uid', valid_map).should be true
          subject.send(:id_in_map?, valid_uid,  'uid', valid_map).should be true
          subject.send(:id_in_map?, valid_both, 'gid', valid_map).should be true
          subject.send(:id_in_map?, valid_gid,  'gid', valid_map).should be true
        end
      end

      context 'with out of range value' do
        it 'should equal false' do
          subject.send(:id_in_map?, invalid_both, 'uid', valid_map).should be false
          subject.send(:id_in_map?, invalid_both, 'gid', valid_map).should be false
        end
      end

      context 'with invalid values' do
        it 'should equal false' do
          subject.send(:id_in_map?, valid_uid, 'uid', invalid_map).should be false
          subject.send(:id_in_map?, valid_uid, 'xid', valid_map).should be false
        end
      end
    end
  end

  context 'with a running machine' do
    let(:container_name) { 'example' }
    let(:container_status) { 'Running' }
    let(:container_address) { '10.0.8.211' }

    let(:container) do
      {
        name: container_name,
        status: container_status,
        stateful: false,
        ephemeral: false,
        profiles: ['default'],
        devices: { root: { path: '/', type: 'disk' } },
        config: { 'volatile.eth0.hwaddr': '00:00:00:00:00:00' },
      }
    end

    let(:container_state) do
      {
        pid: Process.pid,
        status: container_status,
        network: {
          eth0: {
            addresses: [{
              family: 'inet',
              address: container_address,
              netmask: '24',
              scope: 'global',
            }]
          }
        }
      }
    end

    before do
      machine.stub(:id).and_return(container_name)
      lxd.stub(:container).and_return(container)
      lxd.stub(:container_state).and_return(container_state)
    end

    its('state') { should be :running }

    describe 'info' do
      it 'should have a host and port' do
        info = subject.info
        info.should be_a Hash
        info[:host].should eq container_address
        info[:port].should eq 22
      end
    end

    describe 'configure' do
      it 'should update the container' do
        lxd.should receive(:update_container) do |name, update|
          name.should eq container_name
          update.should include container
        end

        subject.configure
      end
    end

    describe 'reconnect' do
      let(:proxy) do
        { type: 'proxy', path: '/var/run/foo' }
      end

      let(:container) do
        super().tap do |container|
          container[:devices].merge!(proxy: proxy)
        end
      end

      it 'should restore proxy devices' do
        lxd.should receive(:update_container) do |name, update|
          name.should eq container_name
          update[:devices].should_not include(proxy: proxy)
        end

        lxd.should receive(:update_container) do |name, update|
          name.should eq container_name
          update[:devices].should include(proxy: proxy)
        end

        subject.reconnect
      end
    end

    describe 'package' do
      include FakeFS::SpecHelpers

      # HACK stop the machine for this test
      # this should be moved into a "stopped machine" context
      let(:container_status) { 'Stopped' }

      let(:image_fingerprint) { SecureRandom.hex(32) }
      let(:image) { { metadata: { fingerprint: image_fingerprint } } }

      it 'should create export directory' do
        lxd.should receive(:create_image_from_container).with(machine.id, kind_of(Hash)).and_return(image)
        lxd.should receive(:export_image) do |fingerprint, output_dir, options|
          fingerprint.should eq image_fingerprint
          output_dir.should be_a String
          options.should include(filename: 'rootfs.tar.gz')
          File.write("#{output_dir}/rootfs.tar.gz", 'wicked cool image content')
        end

        result = subject.package

        Dir.exist?(result).should be true
        Dir.children(result).should match_array ['metadata.json', 'rootfs.tar.gz']

        metadata = JSON.parse(File.read("#{result}/metadata.json"))
        metadata.should include(
          'provider' => 'lxc',
          'version'  => kind_of(String),
          'built-on' => kind_of(String),
        )
      end
    end
  end
end
