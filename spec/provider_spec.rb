#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/action'
require 'lib/vagrant-lxd/provider'

describe VagrantLXD::Provider do
  let(:t) { double('t') }
  let(:machine) { double('machine') }
  let(:machine_info) { Hash(host: '127.0.0.1', port: 22) }
  let(:machine_state) { Vagrant::MachineState::NOT_CREATED_ID }

  subject do
    described_class.new(machine)
  end

  describe 'ssh_info' do
    it 'should return machine info' do
      machine.should_receive(:action).with('info', any_args).and_return(machine_info: machine_info)
      subject.ssh_info.should == machine_info
    end
  end

  describe 'state' do
    it 'should return the machine state' do
      machine.should_receive(:action).with('state', any_args).and_return(machine_state: machine_state)
      I18n.should_receive('t').with('vagrant.commands.status.not_created').and_return(t)
      result = subject.state
      result.should be_a Vagrant::MachineState
      result.id.should be Vagrant::MachineState::NOT_CREATED_ID
      result.short_description.should == 'not created'
      result.long_description.should == t
    end
  end
end
